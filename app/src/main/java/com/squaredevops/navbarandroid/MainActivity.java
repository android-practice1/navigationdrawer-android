package com.squaredevops.navbarandroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    /*
        ANISUL ISLAM
     */

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawerLayout,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // adding toggle button
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        // enable toggle on click listner
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Intent intent;

        if (item.getItemId() == R.id.nav_menu_item_1) {
            intent = new Intent(this, Home.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.nav_menu_item_3) {
            intent = new Intent(this, Chat.class);
            startActivity(intent);
        }

        /*
        int id = item.getItemId();

        if (id == R.id.nav_menu_item_1) {
            Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_menu_item_2) {
            Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_menu_item_3) {
            Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_menu_item_4) {
            Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_menu_item_5) {
            Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);

        return false;
        */
        return false;
    }
}