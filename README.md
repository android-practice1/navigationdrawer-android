# Navigation Drawer

> ## Note
> The ***most easyest*** method to implement navigation drawer. This include Android Navigation Drawer + Header + Navigation Icon = Selected.

<br>

> ## Note
> this one doesn't use fragment. 
> other activities does not have navigation drawer 
> it doesn't contain anything in main window(mainactivity)

### Algorithm 

  * **Class List :**
    * MainActivity
    * Home
    * Chat 
    <br>

  * **Layout List :** 
    * activity_main
    * activity_home
    * activity_chat
    * navigation_header
    * menu/navigation_menu

<br>

  * **MainActivity**
    * Layout ~ *activity_main*
    * onCreate 
    > Added Drawer Layout and Taggle method

    * onOptionsItemSelected
    > To enable taggle in actionbar. 
    ```java
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
    ```
    * onNavigationItemSelected
    > Added activity onclick listner  

  * **Home**
    * Layout ~ *activity_home*
    * Just a simple activity 
    <br>

  * **Chat**
    * Layout ~ *activity_chat*
    * Just a simple activity 

### Dependence 

 * Matetial Design `implementation 'com.google.android.material:material:1.1.0'` 


### Changelogs

  * Git Init 
  * Nothing has changes **just implemented method from page**

### Screenshot 
![luca](app_screenshot.png)

### TESTED AND OK

### EASY BUT NOT GOOD ENOUGH NEED TO WORK MORE@ 

**Source**

[Youtube - Part 1](https://www.youtube.com/watch?v=2aNNoHXOeJs)

[Youtube - Part 2 ](https://www.youtube.com/watch?v=mMr3WRH-abs)

